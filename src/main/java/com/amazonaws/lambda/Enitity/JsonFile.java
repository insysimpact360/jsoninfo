package com.amazonaws.lambda.Enitity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "extractedjson")
public class JsonFile {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String filename;
	private Date createdate;
	private int sitetypeid;
	private String pdfurl;
	private int datatypeid;
	private int statusid;
	private int validatedjsonid;
	private int fileid;
	
	
	public int getDatatypeid() {
		return datatypeid;
	}
	public void setDatatypeid(int datatypeid) {
		this.datatypeid = datatypeid;
	}
	public int getSitetypeid() {
		return sitetypeid;
	}
	public void setSitetypeid(int sitetypeid) {
		this.sitetypeid = sitetypeid;
	}
	public String getPdfUrl() {
		return pdfurl;
	}
	public void setPdfUrl(String pdfUrl) {
		this.pdfurl = pdfUrl;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public int getStatusid() {
		return statusid;
	}
	public void setStatusid(int statusid) {
		this.statusid = statusid;
	}
	public int getValidatedjsonid() {
		return validatedjsonid;
	}
	public void setValidatedjsonid(int validatedjsonid) {
		this.validatedjsonid = validatedjsonid;
	}
	public int getFileid() {
		return fileid;
	}
	public void setFileid(int fileid) {
		this.fileid = fileid;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	
}
