package com.amazonaws.lambda.Enitity;

import java.io.Serializable;
import javax.persistence.*;



@Entity
@Table(name="aws_instances")
public class AwsInstance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="access_key")
	private String accessKey;

	private String name;

	@Column(name="pdf_bucket_name")
	private String pdfBucketName;

	private String region;

	@Column(name="secret_key")
	private String secretKey;

	private String url;
	
	@Column(name="isactive")
	private int isActive;
	
	@Column(name="pdf_graphic_bucket")
	private String pdfGraphicBucket;
	
	@Column(name="graphic_url")
	private String graphicUrl;

	public AwsInstance() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccessKey() {
		return this.accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPdfBucketName() {
		return this.pdfBucketName;
	}

	public void setPdfBucketName(String pdfBucketName) {
		this.pdfBucketName = pdfBucketName;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getSecretKey() {
		return this.secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public String getPdfGraphicBucket() {
		return pdfGraphicBucket;
	}

	public void setPdfGraphicBucket(String pdfGraphicBucket) {
		this.pdfGraphicBucket = pdfGraphicBucket;
	}

	public String getGraphicUrl() {
		return graphicUrl;
	}

	public void setGraphicUrl(String graphicUrl) {
		this.graphicUrl = graphicUrl;
	}

	@Override
	public String toString() {
		return "AwsInstance [id=" + id + ", accessKey=" + accessKey + ", name=" + name + ", pdfBucketName="
				+ pdfBucketName + ", region=" + region + ", secretKey=" + secretKey + ", url=" + url + ", isActive="
				+ isActive + ", pdfGraphicBucket=" + pdfGraphicBucket + ", graphicUrl=" + graphicUrl + "]";
	}

}