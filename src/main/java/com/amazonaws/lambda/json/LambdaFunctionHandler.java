package com.amazonaws.lambda.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.amazonaws.lambda.Enitity.File;
import com.amazonaws.lambda.Enitity.GraphicFile;
import com.amazonaws.lambda.Enitity.JsonFile;
import com.amazonaws.lambda.Hibernate.FileUtils;
import com.amazonaws.lambda.Hibernate.HibernateUtil;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

public class LambdaFunctionHandler implements RequestHandler<S3Event, String> {

	private static Properties prop = new Properties();

	private static final int SITE_TYPE_ID = 1;

	private static final int GRI_TEXT_DATA_TYPE_ID = 2;

	private static final int GRI_TABLE_DATA_TYPE_ID = 1;
	
	private static final String PDF_EXTENSION = ".pdf";
	
	private static final String FILE_PATTERN = "_extxt.json";
	
	private static final int DEFAULT_STATUS = 1;
	
	private static final int DEFAULT_VALIDATED_ID = 0;

	private AmazonS3 s3 = AmazonS3ClientBuilder.standard().build();

	public LambdaFunctionHandler() {
		setProperties();
	}

	// Test purpose only.
	LambdaFunctionHandler(AmazonS3 s3) {
		this.s3 = s3;
	}

	@Override
	public String handleRequest(S3Event event, Context context) {
		context.getLogger().log("Received event: " + event);

		// Get the object from the event and show its content type
		String bucket = event.getRecords().get(0).getS3().getBucket().getName();
		String key = event.getRecords().get(0).getS3().getObject().getKey();
		boolean isExtractedTextData = false;
		boolean isGraphicData = false;
		String contentType = StringUtils.EMPTY;
		try {
			key = URLDecoder.decode(key);
			S3Object response = s3.getObject(new GetObjectRequest(bucket, key));
			key = URLDecoder.decode(response.getKey());
     		context.getLogger().log("Triggered bucket............"+bucket);
     		context.getLogger().log("File Name..................."+key);
			if(StringUtils.equals(bucket, prop.getProperty("aws.s3.graphic.bucket.name"))) {
				isGraphicData = true;
				context.getLogger().log("Graphic Data....................");
			}
			if(!isGraphicData) {
				if (StringUtils.equalsIgnoreCase(bucket, prop.getProperty("aws.s3.text.bucket.name"))
						&& StringUtils.contains(key, FILE_PATTERN)) {
					isExtractedTextData = true;
					key = key.replace("_extxt.json", ".json");
					context.getLogger().log("Extracted text..............");
				}
				int datatypeid = GRI_TABLE_DATA_TYPE_ID;
				if (isExtractedTextData) {
					datatypeid = GRI_TEXT_DATA_TYPE_ID;
				}
				Session session = HibernateUtil.getSessionFactory().openSession();
				
				String filename = key.substring(0,key.lastIndexOf("_PAGE"))+".json";
				
				String url = prop.getProperty("aws.s3.bucket.url") + FilenameUtils.removeExtension(key) + PDF_EXTENSION;
				
				FileUtils.isJsonExists(session, filename, url, datatypeid);
				
				contentType = response.getObjectMetadata().getContentType();
				InputStream in = response.getObjectContent();
				JSONParser jsonParser = new JSONParser();
				JSONObject jsonObject = (JSONObject) jsonParser.parse(new InputStreamReader(in, "UTF-8"));

				session.beginTransaction();

				File file = new File();
				file.setFilecontent(jsonObject.toJSONString());
				session.save(file);
				session.refresh(file);
				
				JsonFile fileObj = new JsonFile();
				fileObj.setFilename(filename);
				fileObj.setFileid(file.getId());
				fileObj.setCreatedate(new Date());
				fileObj.setSitetypeid(SITE_TYPE_ID);
				fileObj.setDatatypeid(datatypeid);
				fileObj.setPdfUrl(url);
				fileObj.setStatusid(DEFAULT_STATUS);
				fileObj.setValidatedjsonid(DEFAULT_VALIDATED_ID);

				// saving as soon
				session.save(fileObj);

				session.getTransaction().commit();
				
			}else {
				Session session = HibernateUtil.getSessionFactory().openSession();
				String url = prop.getProperty("aws.s3.graphic.bucket.url") + key;
				String file = key.substring(0,key.lastIndexOf("_PAGE"))+".pdf";
				
				FileUtils.isGraphicFileExists(session, file, url);
				
				session.beginTransaction();
				GraphicFile fileObj = new GraphicFile();
				
         		fileObj.setFilename(file);
         		fileObj.setStatus(0);
         		fileObj.setPdfurl(url);
         		session.save(fileObj);
         		context.getLogger().log("File Info Is inserted in table  !!");
         		session.getTransaction().commit();
			}
			

			context.getLogger().log("CONTENT TYPE: " + contentType);
			return contentType;
		} catch (Exception e) {
			e.printStackTrace();
			context.getLogger().log(String.format("Error getting object %s from bucket %s. Make sure they exist and"
					+ " your bucket is in the same region as this function.", key, bucket));
			return e.getMessage();

		}finally {
			HibernateUtil.shutdown();
		}
	}

	private void setProperties() {
		String fileName = "/resources/application.properties";
		InputStream is = getClass().getResourceAsStream(fileName);
		if (is == null) {
			throw new IllegalArgumentException("file is not found!");
		} else {
			try {
				prop.load(is);
			} catch (IOException e) {
				throw new IllegalArgumentException("file is not found!");
			}
		}

	}
		
}