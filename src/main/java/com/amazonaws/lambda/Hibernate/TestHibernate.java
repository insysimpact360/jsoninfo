package com.amazonaws.lambda.Hibernate;

import java.util.Date;

import org.hibernate.Session;

import com.amazonaws.lambda.Enitity.JsonFile;

public class TestHibernate {
	
	public static void main(String[] args) {
		
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
       
		JsonFile fileObj = new JsonFile();
		fileObj.setFilename("test");
		//fileObj.setFilecontent("abced");
		fileObj.setCreatedate(new Date());
		
		session.save(fileObj);
		//  Change the hibernateUtil , relative path to get Hibernate.cfg.xml
		session.getTransaction().commit();
		HibernateUtil.shutdown();
	}

}
