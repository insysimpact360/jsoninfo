package com.amazonaws.lambda.Hibernate;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.hibernate.Query;
import org.hibernate.Session;

import com.amazonaws.lambda.Enitity.AwsInstance;
import com.amazonaws.lambda.Enitity.JsonFile;

public class FileUtils {
	
	private static final Logger logger = LogManager.getLogger(FileUtils.class);

	public static void isJsonExists(Session session, String filename, String pdfUrl, Integer datatypeid) {
		Query query = session.createQuery("from AwsInstance where isActive = 1");
		List<AwsInstance> instances = query.list();
		System.out.println(instances);
		if (instances != null && instances.size() > 0) {
			for (AwsInstance instance : instances) {
				deleteJSONFile(session, instance, filename, pdfUrl, datatypeid);
			}
		}
	}

	private static void deleteJSONFile(Session session, AwsInstance instance, String filename, String pdfUrl,
			Integer datatypeid) {
		pdfUrl = instance.getUrl() + pdfUrl;
		session.beginTransaction();
		try {
			Query query = session.createQuery(
					"delete from JsonFile where filename = :filename and sitetypeid = 1 and  pdfurl= :pdfUrl "
							+ "and datatypeid = :datatypeid and validatedjsonid = 0 ");
			query.setParameter("filename", filename);
			query.setParameter("pdfUrl", pdfUrl);
			query.setParameter("datatypeid", datatypeid);
			query.executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error("Error while deleting the json file ........"+filename+" ......... "+pdfUrl);
		}
	}

	public static void isGraphicFileExists(Session session, String filename, String pdfUrl) {
		Query query = session.createQuery("from AwsInstance where isActive = 1");
		List<AwsInstance> instances = query.list();
		if (instances != null && instances.size() > 0) {
			for (AwsInstance instance : instances) {
				deleteGraphicFile(session, instance, filename, pdfUrl);
			}
		}
	}

	private static void deleteGraphicFile(Session session, AwsInstance instance, String filename, String pdfUrl) {
		pdfUrl = instance.getGraphicUrl() + pdfUrl;
		session.beginTransaction();
		try {
			Query query = session
					.createQuery("delete from GraphicFile where filename = :filename and pdfurl = :pdfurl and status = 0");
			query.setParameter("filename", filename);
			query.setParameter("pdfurl", pdfUrl);
			query.executeUpdate();
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error("Error while deleting the graphic file ........"+filename+" ......... "+pdfUrl);
		}
	}
	
}
