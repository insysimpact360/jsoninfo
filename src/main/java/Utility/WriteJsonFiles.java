package Utility;




import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.json.simple.JSONObject;

import com.amazonaws.lambda.Enitity.JsonFile;
import com.amazonaws.lambda.Hibernate.HibernateUtil;

public class WriteJsonFiles {

    public static void listFilesForFolder(final File folder) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
            	Session session = HibernateUtil.getSessionFactory().openSession();
            	try
            	{
            	
           		session.beginTransaction();
            	
            	InputStream is = new FileInputStream(fileEntry);
                String jsonTxt = IOUtils.toString(is, "UTF-8"); 
            	
                JsonFile fileObj = new JsonFile();
        		fileObj.setFilename(fileEntry.getName());
        		//fileObj.setFilecontent(jsonTxt);
        		fileObj.setCreatedate(new Date());
        		fileObj.setSitetypeid(2);
        		fileObj.setPdfUrl("");
        		session.save(fileObj);
        		
        		System.out.println("File saved : " + fileEntry.getName());            	
            }
            	 catch(Exception e)
       		 {
       			
            		 System.out.println(e.getMessage()+"----"+ fileEntry.getName());	 
       		 }
            	finally{
               	if(session!=null){
               		session.close();
               	}
               }
               
            }
        }
    }
    
	public static void main(String[] args) throws IOException {

	    
	    
		Session session = HibernateUtil.getSessionFactory().openSession();


	    final File folder = new File("D://CDP//2017");
	    listFilesForFolder(folder);
	    
	    
	/*    File[] listOfFiles = folder.listFiles();

	    for (int i = 0; i < listOfFiles.length; i++) {
	        if (listOfFiles[i].isFile()) {
	            String fileName = listOfFiles[i].getName();
	            String basename = FilenameUtils.getBaseName(fileName);
	            try {
	            	
	                JSONParser jsonParser = new JSONParser();
                    JSONObject jsonObject = (JSONObject)jsonParser.parse(
                    	      new InputStreamReader(in, "UTF-8"));
	            
	            	
	            	String text = new String(Files.readAllBytes(Paths.get(fileName)), StandardCharsets.UTF_8);

	            
	                byte[] encoded = Files.readAllBytes(Paths.get("D://test//" + listOfFiles[i].getName()));
	                String str = new String(encoded, StandardCharsets.UTF_8);
	                
	            }
	            catch(Exception e)
	            {
	            	
	            }

	}

}*/
	}}
