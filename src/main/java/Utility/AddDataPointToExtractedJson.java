package Utility;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;

import java.util.regex.Pattern;
import com.amazonaws.lambda.Enitity.File;
import com.amazonaws.lambda.Enitity.JsonFile;
import com.amazonaws.lambda.Enitity.Validatedjson;
import com.amazonaws.lambda.Hibernate.HibernateUtil;

public class AddDataPointToExtractedJson {

	public static Pattern YEAR_REGEX_CHARS = Pattern.compile("^(20)\\d{2}$");
	
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

			List<File> filesList = new ArrayList<File>();
			Query query = session.createQuery("from JsonFile  where sitetypeid = 1 and datatypeid = 1 and fileid <>0");
			//Query query = session.createQuery("from Validatedjson  where fileid <> 0");
			List<JsonFile> objectList = query.list();
			//List<Validatedjson> objectList = query.list();
			if (objectList != null && objectList.size() > 0) {
				for (int i = 0 ; i < objectList.size() ; i++) {
					System.out.println("Object List index......"+i);
					//Validatedjson jsonFile = objectList.get(i);
					JsonFile jsonFile = objectList.get(i);
					File file = (File) session.get(File.class, jsonFile.getFileid());
					JSONParser jsonParser = new JSONParser();
					Object obj = jsonParser.parse(file.getFilecontent());
					JSONObject jObj = (JSONObject) obj;
					JSONArray tables = (JSONArray) jObj.get("tables");
					tagTables(tables);
					String content = jObj.toString();
					file.setFilecontent(content);
					System.out.println("File Id ....."+file.getId());
					session.beginTransaction();
					session.saveOrUpdate(file);
					session.getTransaction().commit();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public static void tagTables(JSONArray tables) {
		if (tables != null && tables.size() > 0) {
			tables.forEach(table -> tableRow((JSONObject) table));
		}
	}

	private static void tableRow(JSONObject table) {
		JSONArray rows = (JSONArray) table.get("rows");
		JSONArray tags = (JSONArray) table.get("tags");
		//rows.forEach(row -> tableCell((JSONObject) row, tags));
		if(rows.size() > 0)
		{
			Map<Integer, String> yearMap = getYearInformationForFlagging((JSONObject)rows.get(0));
			if(yearMap != null && yearMap.size() >0) {
				flagYearData(rows, yearMap);
			}
		}
	}

	private static void flagYearData(JSONArray rows, Map<Integer, String> yearMap) {
		for ( int i=1; i<rows.size(); i++) {
			JSONObject row = (JSONObject) rows.get(i);
			JSONArray cells = (JSONArray) row.get("cells");
			boolean matched = false;
			for(int x=0; x< cells.size(); x++) {
				JSONObject cell = (JSONObject)cells.get(x);
				String kpi = (String) cell.get("kpi");
				if(StringUtils.isNotBlank(kpi)) {
					matched = true;
					break;
				}
			}
			if(matched) {
				 for (Map.Entry<Integer,String> entry : yearMap.entrySet()) {
					 JSONObject cell = null;
					 try {
						 cell = (JSONObject)cells.get(entry.getKey());
					 }catch(Exception e) {
						 continue;
					 }
					 String cellData = (String) cell.get("columnData");
					 cellData = StringUtils.replace(cellData, ",", "");
					 cellData = Jsoup.parse(cellData).text();
					 if(NumberUtils.isCreatable(cellData)) {
						 cell.put("datapoint", entry.getValue());
					 }
				 }
			}
			else {
				continue;
			}
		}
		
	}

	
	private static Map<Integer, String> getYearInformationForFlagging(JSONObject firstRow){
		Map<Integer, String> yearMap = new HashMap<Integer, String>();
		JSONArray cells = (JSONArray) firstRow.get("cells");
		
		for (int i =0; i < cells.size(); i++) {
			JSONObject cell = (JSONObject)cells.get(i);
			String cellData = (String) cell.get("columnData");
			cellData = cellData.replaceAll("[^a-zA-Z0-9]", "");
			if(StringUtils.isNotBlank(cellData) && YEAR_REGEX_CHARS.matcher(cellData.trim()).matches()) {
				yearMap.put(i, cellData.trim());
			}
		}
		return yearMap;
	}

}
