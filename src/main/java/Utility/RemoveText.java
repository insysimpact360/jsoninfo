package Utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import org.apache.pdfbox.pdfwriter.ContentStreamWriter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;

public class RemoveText {

    public static final String relativeInputPath = "src/main/resources/InputFiles/";
    public static final String relativeOutputPath = "src/main/resources/OutputFiles/";

    public static PDDocument removeText(PDPage page, int pageNum) throws IOException {
        PDFStreamParser parser = new PDFStreamParser(page);
        List<Object> tokens = parser.getTokens();
        List<Object> newTokens = new ArrayList<>();
        parser.parse();
        final List<String> PAINTING_PATH_OPS = Arrays.asList("Tf", "Tw", "Td", "Tm");
        final List<String> graphsOperator = Arrays.asList("q", "Q", "cm", "w", "j", "M", "d", "ri", "gs");
        for (Object token : tokens) {
            if (token instanceof Operator) {
                Operator op = (Operator) token;
                //  if (Pattern.matches("GS*",  op.getName()) || op.getName().equals("gs") || op.getName().equals("Tj") ||op.getName().equals("TJ") || graphsOperator.contains(op.getName())  ) {
                if (op.getName().equals("Tj") || op.getName().equals("TJ") || op.getName().equals("Do")) {
                    newTokens.remove(newTokens.size() - 1);
                    continue;
                }
            }
            newTokens.add(token);
        }


        PDDocument document = new PDDocument();
        document.addPage(page);

        PDStream newContents = new PDStream(document);
        OutputStream out = newContents.createOutputStream(COSName.FLATE_DECODE);
        ContentStreamWriter writer = new ContentStreamWriter(out);
        writer.writeTokens(newTokens);
        out.close();

        page.setContents(newContents);

        return document;

    }

    public static void main(String[] args) throws IOException {

        RemoveText removeText = new RemoveText();
        removeText.process();


    }

    private void process() throws IOException {
        //  OperatorName.SHOW_TEXT_LINE;
        PDDocument document = null;
        String fileName = relativeInputPath + "Buzz_Annual_Park.pdf";
        try {

            document = PDDocument.load(new File(fileName));
            PdfProcessor printer = new PdfProcessor();
            int pageNum = 0;
            for (PDPage page : document.getPages()) {
                PDPage updatedPage = page;
                pageNum++;
                System.out.println("Processing page: " + pageNum);

                PDDocument doc = removeText(page, pageNum);

                File f = new File(relativeOutputPath + "RemoveTextFiles/Buzz_Annual_Park" + pageNum + ".pdf");
                FileOutputStream fOut = new FileOutputStream(f);
                doc.save(fOut);

            }

        } catch (InvalidPasswordException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (document != null) {
                document.close();
            }
        }
    }
}

