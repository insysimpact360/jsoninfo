package Utility;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.amazonaws.lambda.Enitity.JsonFile;
import com.amazonaws.lambda.Hibernate.HibernateUtil;

public class ExtractedPDFUrl {
	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			SQLQuery sites_query = session.createSQLQuery(
					"select * from extractedjson where pdfUrl like ('https://impact360graphicbucket.s3.amazonaws.com/%')");
			List<JsonFile> list = getExtractJsonList(sites_query.list());
			if (list != null && list.size() > 0) {
				session.beginTransaction();
				for (JsonFile jsonfile : list) {
					String pdf = jsonfile.getPdfUrl();
					pdf = pdf.replace("https://insysdevtestbucket-2.s3.amazonaws.com/",
							"https://insysdevtestbucket-4.s3.amazonaws.com/");
					jsonfile.setPdfUrl(pdf);
					session.saveOrUpdate(jsonfile);
				}
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			HibernateUtil.shutdown();
		}
	}

	private static List<JsonFile> getExtractJsonList(List<Object[]> objectList) {
		List<JsonFile> siteList = new ArrayList<JsonFile>();
		JsonFile sites = null;
		if (objectList != null && !objectList.isEmpty()) {
			for (Object[] obj : objectList) {
				sites = new JsonFile();
				sites.setId((Integer) obj[0]);
				sites.setFilename((String) obj[1]);
				//sites.setFilecontent((String) obj[2]);
				sites.setSitetypeid((Integer) obj[3]);
				sites.setDatatypeid((Integer) obj[4]);
				sites.setCreatedate((Date) obj[5]);
				sites.setPdfUrl((String) obj[6]);
				siteList.add(sites);
			}
		}
		return siteList;
	}
}
