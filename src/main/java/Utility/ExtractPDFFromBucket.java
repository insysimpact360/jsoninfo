package Utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.parser.ParseException;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class ExtractPDFFromBucket {

	public static void main(String arg[]) throws UnsupportedEncodingException, IOException, ParseException {

		AWSCredentials awsCredentials = new BasicAWSCredentials("AKIAXM4R44W7GACEBYM2",
				"MrJ9f55dMgiGqBwhS/3U3kOdSUR2aT7tlZNzz+F9");
		AmazonS3ClientBuilder builder = AmazonS3ClientBuilder.standard();
		builder.withCredentials(new AWSStaticCredentialsProvider(awsCredentials));
		builder.setRegion("us-east-1");
		AmazonS3 amazonS3Client = builder.build();
		String bucketName = "impact360graphicbucket"; // "impact360jsonbucket";
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName);
		List<S3Object> jsonObjectList = new ArrayList<S3Object>();
		ObjectListing objects = amazonS3Client.listObjects(listObjectsRequest);

		List<S3ObjectSummary> summaries = objects.getObjectSummaries();

		for (S3ObjectSummary obje : summaries) {
			if (obje.getKey().endsWith(".pdf")) {

				S3Object response = amazonS3Client.getObject(obje.getBucketName(), obje.getKey());
				InputStream inputStream = null;

				try {
					String pdfFile = obje.getKey();

					String contentType = response.getObjectMetadata().getContentType();
					inputStream = response.getObjectContent();
					File targetFile = new File("D:\\PDF_FOLDER\\"+bucketName+"\\" + pdfFile);

					try (FileOutputStream outputStream = new FileOutputStream(targetFile)) {

						int read;
						byte[] bytes = new byte[1024];

						while ((read = inputStream.read(bytes)) != -1) {
							outputStream.write(bytes, 0, read);
						}

					}
				} finally {
					if (response != null) {
						response.close();
					}
					if (inputStream != null) {
						inputStream.close();
					}
				}

			}

		}
	}
}
