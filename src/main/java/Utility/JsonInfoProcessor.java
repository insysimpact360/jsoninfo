package Utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.hibernate.Session;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.lambda.Enitity.JsonFile;
import com.amazonaws.lambda.Hibernate.HibernateUtil;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;

public class JsonInfoProcessor {
	
	public static void main(String arg[]) throws UnsupportedEncodingException, IOException, ParseException
	{

        AWSCredentials awsCredentials= new BasicAWSCredentials("AKIAXM4R44W7GACEBYM2","MrJ9f55dMgiGqBwhS/3U3kOdSUR2aT7tlZNzz+F9");
        AmazonS3ClientBuilder builder = AmazonS3ClientBuilder.standard();
        builder.withCredentials(new AWSStaticCredentialsProvider(awsCredentials));
        builder.setRegion("us-east-1");
        AmazonS3 amazonS3Client = builder.build();
        String bucketName = "impact360jsonbucket";
        ListObjectsRequest listObjectsRequest =  new ListObjectsRequest().withBucketName(bucketName);
        List<S3Object> jsonObjectList = new ArrayList<S3Object>();
        ObjectListing objects = amazonS3Client.listObjects(listObjectsRequest);
        
            List<S3ObjectSummary> summaries = objects.getObjectSummaries();

       		Session session = HibernateUtil.getSessionFactory().openSession();
       		session.beginTransaction();
            
         //   summaries.stream().filter(s->s.getKey().endsWith(".json")).
        //    forEach(s ->jsonObjectList.add(amazonS3Client.getObject(s.getBucketName(), s.getKey())));
            
            
            for(S3ObjectSummary obje :summaries)
            {
            	if(obje.getKey().endsWith(".json"))
            	{
            		S3Object response = amazonS3Client.getObject(obje.getBucketName(),obje.getKey());	
            		 try {
            			 //jsonObjectList.add(object);
            			 
            			  
                     	
                     	String contentType = response.getObjectMetadata().getContentType();
                         InputStream in = response.getObjectContent();
                         JSONParser jsonParser = new JSONParser();
                         JSONObject jsonObject = (JSONObject)jsonParser.parse(
                         	      new InputStreamReader(in, "UTF-8"));
                         
                		JsonFile fileObj = new JsonFile();
                		fileObj.setFilename(response.getKey());
                		//fileObj.setFilecontent(jsonObject.toJSONString());
                		fileObj.setCreatedate(new Date());
                		fileObj.setSitetypeid(1);
                		fileObj.setPdfUrl("https://insysdevtestbucket.s3.amazonaws.com/"+FilenameUtils.removeExtension(response.getKey())+".pdf");
                		session.save(fileObj);
                     
                		
                       }
            		 finally{
                       	if(response!=null){
                       		response.close();
                       	}
                       }
                       
            	}
            }
            
           
            
            
            
            

		/*
		 * for(S3Object response : jsonObjectList) {
		 * 
		 * String contentType = response.getObjectMetadata().getContentType();
		 * InputStream in = response.getObjectContent(); JSONParser jsonParser = new
		 * JSONParser(); JSONObject jsonObject = (JSONObject)jsonParser.parse( new
		 * InputStreamReader(in, "UTF-8"));
		 * 
		 * JsonFile fileObj = new JsonFile(); fileObj.setFilename(response.getKey());
		 * fileObj.setFilecontent(jsonObject.toJSONString()); fileObj.setCreatedate(new
		 * Date()); session.save(fileObj); }
		 */
       		// saving as soon
       		
       		
       		session.getTransaction().commit();
       		HibernateUtil.shutdown();
          
        	
        		
	}

}
